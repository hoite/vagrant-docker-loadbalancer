# Vagrant Docker Nginx-Loadbalancer

## Preperation
install Vagrant Provisioner: Docker Compose
use following command:

```bash
vagrant plugin install vagrant-docker-compose
```
from: https://github.com/leighmcculloch/vagrant-docker-compose

## Usage

```bash
git clone https://gitlab.com/hoite/vagrant-docker-loadbalancer
cd vagrant-docker-loadbalancer
vagrant up
```

Wijziging
